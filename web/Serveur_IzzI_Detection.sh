#Ask for update and upgrade
echo "\e[31;1mAutoinstallpack \e[0m"
sleep 0.5
echo  "\e[33;1mUpdate and upgrade ?  [Y/n] \e[0m"
read repUp
case $repUp in
   y|Y|yes|YES|Yes|"") sudo apt-get update -y && sudo apt-get upgrade -y
      ;;
   n|N|NO|No|no|*) echo -e "\e[31;1mNO Update and Upgrade\e[0m"
      ;;
esac
echo "\e[31;1mInstall Apache2 server \e[0m"
sudo apt-get install -y apache2
echo "\e[31;1mInstall PHP \e[0m"
sudo apt-get install -y libapache2-mod-php
echo "\e[31;1mInstall PDO_SQLite driver \e[0m"
sudo apt-get install -y php7.4-sqlite
echo "\e[31;1mInstall mplayer \e[0m"
sudo apt install -y mplayer
echo "\e[31;1mAdd www-data in audio group \e[0m"
sudo adduser www-data audio
echo "\e[31;1mStart apache server \e[0m"
sudo systemctl start apache2
