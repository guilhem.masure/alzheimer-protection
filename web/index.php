<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <!--<link rel="stylesheet" type="text/css" href="css/style.css" />
        -->
        <link rel="icon" type="image/png" href="img/param.png" />
        <title>Setup IzzI-Detection</title>
    </head>
    <body>
        <header>
            <form NAME="Language">
                <select NAME="liste_langage" onChange="change()"  id="language">
                  <option value=""> Choix de la langue </option>
                  <option value="index.html">Francais</option>
                  <option value="indexEN.html">Anglais</option>
                </select>
            </form>
            <h1>IzzI-Detection configuration</h1>
        </header>
        <div class="formulairePower">
            <form action="Power.php" method="POST" name="Power">
                <p>Please select the status of the detection device :</p>
                <div class="formPower">
                    <input type="radio" id="Power1" name="state" value="ON">
                    <label for="Power1">Allumé</label>
                    <input type="radio" id="Power2" name="state" value="OFF">
                    <label for="Power2">Eteint</label>
                </div>
                <input type="submit" value="Submit" id="submitPower">
            </form>
        </div>
        <div class="Mesure">
            <form action="Mesure.php" method="POST" name="Mesure">
                Press to recalculate the detection distance : <input type="submit" value="Recalcul">
            </form>
        </div>
        <?php
	try{
                $connexion=new PDO('sqlite:./IzzI_Detection.db');
            }
            catch(PDOException $e){
                printf("Connection failed : %s\n", $e->getMessage());
                exit();
            }
            $sql="SELECT COUNT(*) from device";
            if(!$connexion->query($sql)) echo "device access problem";
            else{
                $res = $connexion->query($sql);
                $count = $res->fetchColumn();
                $id = $connexion->query("SELECT * from device");
                echo 'Section for setting up your '.$count.' currently connected devices<br/>';
            }
            while($row = $id->fetch())
            {
                if(strcmp($row['Name'],'to set up') == 0)
                {
                    echo "<p>We will set up the name of your new device: </p>";
                    echo "
                    <form action='name.php' method='post'>
                        <input type='hidden' name='id' value='".$row['Id'] ."' />
                        New device name :  <input type='text' name='name' /><input type='submit' value='OK'>
                    </form>";
                }
                else
                {
                    echo
                    "<p>Device configuration ".$row['Name']." :<br/>";
                    echo '
                    <form enctype="multipart/form-data" action="uploader.php" method="POST">
                        <input type="hidden" name="id" value="'.$row['Id'] .'" />
                        Choose a file to upload: <input name="uploadedfile" type="file" /><br />
                        <input type="submit" value="Upload File" />
                    </form>';
                }
  
            }
        ?>
        <script src="JS/language.js"></script>
    </body>
</html>
