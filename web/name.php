<?php
    $dsn="sqlite:./IzzI_Detection.db";
    try{
        $connexion=new PDO($dsn);
    }
    catch(PDOException $e){
        printf("Connection failed : %s\n", $e->getMessage());
        exit();
    }
    $stmt = $connexion->prepare("UPDATE device SET name=? where id=?");
    $name = $_POST['name'];
    $id = $_POST['id'];
    $stmt->execute([$name, $id]);
    echo "<p>The name of your device has been changed. New name :" .$name ."<p/>";
    echo "<p>you will be redirected<p/>";
    $delai = 5; 
    $url = './index.php';
    header("Refresh: $delai;url=$url");
?>
