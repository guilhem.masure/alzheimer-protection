#include <HCSR04.h> //sensor
#include <WiFi.h> //communication
#include <ESP32Servo.h> //alarm

//FIXED TECHNICAL
UltraSonicDistanceSensor distanceSensor(14, 12);
const byte ledPin = 18;
const byte interruptPin = 19;
const byte alarmPin = 27;

//FIXED UTILITARY
float detectionDistance;
volatile byte buttonState;
volatile int startTimer, stopTimer;
boolean shortPress = false;
boolean longPress = false;

//COMMUNICATION
char ipRaspberry[16] = "10.42.0.1";
byte macArduinoByte[6];
char macArduino[18];
WiFiClient client;
int status = WL_IDLE_STATUS;

char ssid[50] = "IzzI_Detection";
char pass[50] = "Protection_Alzheimer_Project2021";
char getRequestDeviceConnect[50] = "GET /device.php?id=";
char getRequestAlarmOn[50] = "GET /alert.php?id=";
char getRequestAlarmOff[50] = "GET /stopAlert.php?id=";
char hostRequest[50] = "Host: ";

//CONFIGURABLE DURATIONS (MS)
int detectionDelay = 200;
int offDurationShortPress = 5000;
int offDurationLongPress = 12500;
int shortPressMinDuration = 100;
int longPressMinDuration = 1500;

//CONFIGURABLE DISTANCES (CM)
int detectionConfidenceInterval = 10;



void setup() {
  //Initialisation of communication with the serial monitor
  Serial.begin(115200); 
  pinMode(ledPin, OUTPUT);
  pinMode(interruptPin, INPUT_PULLUP);


  //Program interruption from button interaction
  attachInterrupt(digitalPinToInterrupt(interruptPin), blink, CHANGE);

  //Detection distance measurement
  detectionDistance = distanceSensor.measureDistanceCm();

  //WiFi setup
  /*if (WiFi.status() == WL_NO_SHIELD) {
    Serial.println("WiFi shield not present");
    while (true);
  }*/

  //Arduino mac address recovery & GET request setup
  WiFi.macAddress(macArduinoByte);
  sprintf(macArduino, "%02u%02u%02u%02u%02u%02u",macArduinoByte[0],macArduinoByte[1],macArduinoByte[2],macArduinoByte[3],macArduinoByte[4],macArduinoByte[5]);
  strcat(getRequestDeviceConnect,macArduino);
  strcat(getRequestAlarmOn,macArduino);
  strcat(getRequestAlarmOff,macArduino);
  strcat(hostRequest,ipRaspberry);

  //Attempt to connect to Wifi network
  while (status != WL_CONNECTED) {
    Serial.print("Attempting to connect to SSID: ");
    Serial.println(ssid);
    Serial.println(getRequestDeviceConnect);
    //Connecting to WPA/WPA2 network
    status = WiFi.begin(ssid, pass);
    delay(10000);
  }
  Serial.println("Connected to WiFi");
  printWifiStatus();
  Serial.println("\nStarting connection to server...");
  if (client.connect(ipRaspberry, 80)) {
    Serial.println("Connected to HTTP server");
  }

  //GET request device connect
  if (client.connect(ipRaspberry,80)) {
    client.println(getRequestDeviceConnect);
    client.println(hostRequest);
    client.println("Connection: close");
    client.println();
  }
}



void loop() {
  //Obstacle checking
  if (distanceSensor.measureDistanceCm() > detectionDistance + detectionConfidenceInterval || distanceSensor.measureDistanceCm() < detectionDistance - detectionConfidenceInterval)
  {
    alarmeObstacle();
  }

  //Button press checking
  if (shortPress) {
    tone(alarmPin,261.626,15);
    delay(offDurationShortPress);
    shortPress = false;
  }
  if (longPress) {
    tone(alarmPin,261.626,15);
    delay(30);
    tone(alarmPin,261.626,15);
    delay(offDurationLongPress);
    detectionDistance = distanceSensor.measureDistanceCm();
    longPress = false;
  }

  if (Serial.available() )
  {
      if(Serial.read()=='s') {
        shortPress = true;
        Serial.println("shortPress");
      } else {
        if(Serial.read()=='l') {
          longPress = true;
          Serial.println("longPress");
        }
      }
      
  }
  
  delay(detectionDelay);
}



void printWifiStatus() {
  Serial.print("SSID: ");
  Serial.println(WiFi.SSID());
  IPAddress ip = WiFi.localIP();
  Serial.print("IP Address: ");
  Serial.println(ip);
  long rssi = WiFi.RSSI();
  Serial.print("Signal strength (RSSI):");
  Serial.print(rssi);
  Serial.println(" dBm");
}



void blink() {
  buttonState = !buttonState;
  if (!buttonState) {
    appuiBouton();
  }
  else {
    relacheBouton();
  }
}



void appuiBouton() {
  startTimer = millis();
}



void relacheBouton() {
  stopTimer = millis() - startTimer;
  if (stopTimer < longPressMinDuration && stopTimer > shortPressMinDuration) {
    shortPress = true;
  }
  else {
    longPress = true;
  }
  buttonState = HIGH;
}



void alarmeObstacle() {
  Serial.print("Obstacle");
  //Main alarm ON request
  if (client.connect(ipRaspberry,80)) {
    client.println(getRequestAlarmOn);
    client.println(hostRequest);
    client.println("Connection: close");
    client.println();
  }
  //Alarm until button press
  while (!shortPress) {
    tone(alarmPin,220,500);
    delay(20);
    tone(alarmPin,277.183,500);
    delay(20);
    tone(alarmPin,329.628,500);
    delay(1000);
    
    if (Serial.available() )
    {
      if(Serial.read()=='s') {
        shortPress = true;
        Serial.println("shortPress");
      }
     }
  }
  //Main alarm OFF request
  if (client.connect(ipRaspberry,80)) {
    client.println(getRequestAlarmOff);
    client.println(hostRequest);
    client.println("Connection: close");
    client.println();
  }
}
